#!/bin/bash

exec 3< $1
exec 4> $2
temp=0
temp2=0
tpid=0
thid=0

while read -u 3 -r hid pid a g
do
	if [ "$g" == "2" ] && [ $a -gt 30 ] && [ $a -lt 50 ]; then
		tpid=$(( tpid+1 ))
		echo $pid >&4

		if [ $temp2 == $temp ] && [ $temp2 == 0 ]; then
		thid=$(( thid+1 ))
		temp2=0
		temp=0
		fi

		if [ $temp == $pid ]; then
		temp2=$pid
		else
		temp=$pid
		fi
	fi
done

echo Total extracted person ids: $tpid
echo Total household with 3 or more PID: $thid
exec 3<&-
exec 4>&-

